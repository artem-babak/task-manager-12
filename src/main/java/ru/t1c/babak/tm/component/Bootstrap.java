package ru.t1c.babak.tm.component;

import ru.t1c.babak.tm.api.controller.ICommandController;
import ru.t1c.babak.tm.api.controller.IProjectController;
import ru.t1c.babak.tm.api.controller.ITaskController;
import ru.t1c.babak.tm.api.repository.ICommandRepository;
import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.service.ICommandService;
import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.api.service.ITaskService;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.controller.CommandController;
import ru.t1c.babak.tm.controller.ProjectController;
import ru.t1c.babak.tm.controller.TaskController;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.repository.ProjectRepository;
import ru.t1c.babak.tm.repository.TaskRepository;
import ru.t1c.babak.tm.service.CommandService;
import ru.t1c.babak.tm.service.ProjectService;
import ru.t1c.babak.tm.service.TaskService;
import ru.t1c.babak.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        initData();
        predefinedCommands();
        commandController.showWelcome();
        while (true) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            runCommand(command);
        }
    }

    private void initData() {
        taskService.create("Demo_task-1", "first demo task");
        taskService.create("DEMO_TASK-2", "second demo task");
        taskService.create("demo_task-3");
        taskService.updateOneByIndex(2, "demo_task-3.1", "new description");
        projectService.create("proj-1", "first demo proj");
        projectService.create("proj-2", "second demo proj");
        projectService.create("proj-3");
        projectService.updateOneByIndex(2, "proj-3.1", "new description");
    }

    private void predefinedCommands() {
        runCommand("version");
        runCommand("task-list");
        runCommand("project-list");
    }

    public boolean runArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runArgument(arg);
        return true;
    }

    public void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void close() {
        System.exit(0);
    }

}
