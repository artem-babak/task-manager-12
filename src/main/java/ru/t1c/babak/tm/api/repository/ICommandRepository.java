package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
