package ru.t1c.babak.tm.controller;

import ru.t1c.babak.tm.api.controller.ITaskController;
import ru.t1c.babak.tm.api.service.ITaskService;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Task;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        for (int i = 0; i < tasks.size(); ) {
            final Task task = tasks.get(i++);
            if (task == null) continue;
            System.out.println("\t" + i + ". " + task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        System.out.println("\tID: " + task.getId());
        System.out.println("\tNAME: " + task.getName());
        System.out.println("\tDESCRIPTION: " + task.getDescription());
        final Status status = task.getStatus();
        if (status != null) System.out.println("\tSTATUS: " + status.getDisplayName());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("\tENTER ID:");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        task = taskService.updateOneById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("\tENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        task = taskService.updateOneByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            final Status status = Status.valueOf(statusValue);
            taskService.changeTaskStatusById(id, status);
            System.out.println("[OK]");
        } catch (IllegalArgumentException e) {
            System.out.println("[FAIL]");
        }
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            final Status status = Status.valueOf(statusValue);
            taskService.changeTaskStatusByIndex(index, status);
            System.out.println("[OK]");
        } catch (IllegalArgumentException e) {
            System.out.println("[FAIL]");
        }
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        taskService.changeTaskStatusById(id, Status.COMPLETED);
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        System.out.println("[OK]");
    }

}
